package com.dharbor.talent.managervacations.holiday.service.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * @author Jhonatan Soto
 */
@Getter
@Setter
@AllArgsConstructor
public class CommonResponse {
    private String statusCode;
    private String message;
}

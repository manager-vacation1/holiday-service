package com.dharbor.talent.managervacations.holiday.service.dto.response;

import com.dharbor.talent.managervacations.holiday.service.constant.ResponseConstant;
import com.dharbor.talent.managervacations.holiday.service.domain.CommonResponse;
import com.dharbor.talent.managervacations.holiday.service.domain.Holiday;
import com.dharbor.talent.managervacations.holiday.service.dto.HolidayDto;
import lombok.Getter;
import lombok.Setter;

/**
 * @author Jhonatan Soto
 */

@Setter
@Getter
public class HolidayResponse extends CommonResponse {

    private HolidayDto holiday;

    public HolidayResponse(HolidayDto holiday) {
        super(ResponseConstant.StatusCodeResponse.SUCCESS_CODE, ResponseConstant.StatusCodeResponse.SUCCESS_MSG);
        this.holiday = holiday;
    }
}

package com.dharbor.talent.managervacations.holiday.service.service;

import com.dharbor.talent.managervacations.holiday.service.common.Message;
import com.dharbor.talent.managervacations.holiday.service.domain.Holiday;
import com.dharbor.talent.managervacations.holiday.service.dto.CountryDto;
import com.dharbor.talent.managervacations.holiday.service.exception.BadRequestException;
import com.dharbor.talent.managervacations.holiday.service.repository.HolidayRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

/**
 * @author Jhonatan Soto
 */
@Service
@AllArgsConstructor
public class HolidayServiceImpl implements IHolidayService {

    private HolidayRepository holidayRepository;

    private IFeignCountry feignCountry;

    private Message message;

    @Override
    public Optional<Holiday> findById(Long id) {
        return holidayRepository.findById(id);
    }

    @Override
    public List<Holiday> findAllByCountryId(Long id) {
        return holidayRepository.findAllByCountryId(id);
    }

    @Override
    public Holiday save(Holiday holiday) {
        if (holidayRepository.existsByDateAndCountryId(holiday.getDate(), holiday.getCountryId())) {
            throw new BadRequestException(message.getMessage("Same.Date.Vacation"));
        }
        return holidayRepository.save(holiday);
    }

    @Override
    public void deleteById(Long id) {
        holidayRepository.deleteById(id);
    }

    @Override
    public List<Holiday> findByMonthAndCountryId(Integer m, Long id) {
        return holidayRepository.findByMonthAndCountryId(m, id);
    }

    @Override
    public boolean existsByDateAndCountryId(Date date, Long countryId) {
        return holidayRepository.existsByDateAndCountryId(date, countryId);
    }


}

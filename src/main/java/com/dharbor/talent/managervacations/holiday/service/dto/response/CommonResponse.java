package com.dharbor.talent.managervacations.holiday.service.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Jhonatan Soto
 */
@Getter
@Setter
@NoArgsConstructor
public class CommonResponse {
    private String statusCode;
    private String message;
}

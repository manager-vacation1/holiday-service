package com.dharbor.talent.managervacations.holiday.service.mapper;

import com.dharbor.talent.managervacations.holiday.service.domain.Holiday;
import com.dharbor.talent.managervacations.holiday.service.dto.HolidayDto;
import org.mapstruct.Mapper;

import java.util.List;

/**
 * @author Jhonatan Soto
 */
@Mapper(componentModel = "spring")
public interface HolidayStructMapper {
    HolidayDto holidayToHolidayDto(Holiday holiday);

    List<HolidayDto> holidayToHoliday(List<Holiday> holidays);
}

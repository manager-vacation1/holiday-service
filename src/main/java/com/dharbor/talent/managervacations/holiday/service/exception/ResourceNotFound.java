package com.dharbor.talent.managervacations.holiday.service.exception;

/**
 * @author Jhonatan Soto
 */
public class ResourceNotFound extends RuntimeException {
    public ResourceNotFound(String message) {
        super(message);
    }
}

package com.dharbor.talent.managervacations.holiday.service.repository;

import com.dharbor.talent.managervacations.holiday.service.domain.Holiday;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

/**
 * @author Jhonatan Soto
 */
@Repository
public interface HolidayRepository extends JpaRepository<Holiday, Long> {

    List<Holiday> findAllByCountryId(Long id);

    @Query(nativeQuery = true, value = "select * from holiday_table where month(holiday_date) = ?1 and country_id = ?2")
    List<Holiday> findByMonthAndCountryId(Integer m, Long id);

    boolean existsByDateAndCountryId(Date date, Long countryId);


}

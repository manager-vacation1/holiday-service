package com.dharbor.talent.managervacations.holiday.service.dto;

import lombok.*;

/**
 * @author Jhonatan Soto
 */
@Getter
@Setter
@NoArgsConstructor
public class CountryDto {
    private Long id;
    private String code;
    private String name;
}

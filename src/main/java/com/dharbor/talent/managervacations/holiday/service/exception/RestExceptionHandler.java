package com.dharbor.talent.managervacations.holiday.service.exception;

import com.dharbor.talent.managervacations.holiday.service.constant.StatusCode;
import com.dharbor.talent.managervacations.holiday.service.domain.CommonResponse;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.persistence.EntityNotFoundException;

/**
 * @author Jhonatan Soto
 */
@RestControllerAdvice
public class RestExceptionHandler {

    @ExceptionHandler(value = {Exception.class})
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ResponseEntity<CommonResponse> unknownException(Exception ex){
        return new ResponseEntity<>(new CommonResponse(StatusCode.INTERNAL_EXCEPTION.get(),
                ex.getMessage()),
                new HttpHeaders(),
                HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(value = {BadRequestException.class})
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ResponseEntity<CommonResponse> badRequestException(BadRequestException ex) {
        return new ResponseEntity<>(new CommonResponse(StatusCode.BAD_REQUEST_EXCEPTION.get(),
                ex.getMessage()),
                new HttpHeaders(),
                HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = {EntityNotFoundException.class})
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ResponseEntity<CommonResponse> countryNotFound( EntityNotFoundException ex) {
        return new ResponseEntity<>(new CommonResponse(StatusCode.RESOURCE_NOT_FOUND_EXCEPTION.get(),
                ex.getMessage()),
                new HttpHeaders(),
                HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(value = {ResourceNotFound.class})
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ResponseEntity<CommonResponse> countryNotFound( ResourceNotFound ex) {
        return new ResponseEntity<>(new CommonResponse(StatusCode.RESOURCE_NOT_FOUND_EXCEPTION.get(),
                ex.getMessage()),
                new HttpHeaders(),
                HttpStatus.NOT_FOUND);
    }
}

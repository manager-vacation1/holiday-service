package com.dharbor.talent.managervacations.holiday.service.service;

import com.dharbor.talent.managervacations.holiday.service.domain.Holiday;
import org.springframework.data.repository.query.Param;

import java.util.Date;
import java.util.List;
import java.util.Optional;

/**
 * @author Jhonatan Soto
 */
public interface IHolidayService {
    Optional<Holiday> findById(Long id);

    List<Holiday> findAllByCountryId(Long id);

    Holiday save(Holiday holiday);

    void deleteById(Long id);

    List<Holiday> findByMonthAndCountryId(Integer m, Long id);

    boolean existsByDateAndCountryId(Date date, Long countryId);



}

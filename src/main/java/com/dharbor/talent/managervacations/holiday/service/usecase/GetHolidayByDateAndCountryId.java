package com.dharbor.talent.managervacations.holiday.service.usecase;

import com.dharbor.talent.managervacations.holiday.service.domain.Holiday;
import com.dharbor.talent.managervacations.holiday.service.dto.response.HolidayExistResponse;
import com.dharbor.talent.managervacations.holiday.service.service.IHolidayService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Date;

/**
 * @author Jhonatan Soto
 */
@Service
@AllArgsConstructor
public class GetHolidayByDateAndCountryId {

    private IHolidayService iHolidayService;

    public HolidayExistResponse execute(Date date, Long countryId) {
        boolean existHoliday = iHolidayService.existsByDateAndCountryId(date, countryId);
        return new HolidayExistResponse(existHoliday);
    }
}

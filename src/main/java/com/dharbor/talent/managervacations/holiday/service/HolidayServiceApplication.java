package com.dharbor.talent.managervacations.holiday.service;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableEurekaClient
@EnableFeignClients
public class HolidayServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(HolidayServiceApplication.class, args);
    }

}

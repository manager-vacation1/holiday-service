package com.dharbor.talent.managervacations.holiday.service.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

/**
 * @author Jhonatan Soto
 */
@Entity
@Getter
@Setter
@Table(name = ConstantsTableNames.HolidayTable.Name)
public class Holiday {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = ConstantsTableNames.HolidayTable.Id.NAME)
    private Long id;

    @Temporal(TemporalType.DATE)
    @Column(name = ConstantsTableNames.HolidayTable.Date.NAME,
            nullable = false)
    private Date date;

    @Column(name = ConstantsTableNames.HolidayTable.Reason.NAME,
            length = ConstantsTableNames.HolidayTable.Reason.LENGTH,
            nullable = false)
    private String reason;

    @Column(name = ConstantsTableNames.HolidayTable.Country.NAME, nullable = false)
    private Long countryId;

}

package com.dharbor.talent.managervacations.holiday.service.usecase;

import com.dharbor.talent.managervacations.holiday.service.domain.Holiday;
import com.dharbor.talent.managervacations.holiday.service.dto.CountryDto;
import com.dharbor.talent.managervacations.holiday.service.dto.response.GetHolidaysResponse;
import com.dharbor.talent.managervacations.holiday.service.mapper.HolidayStructMapper;
import com.dharbor.talent.managervacations.holiday.service.service.IFeignCountry;
import com.dharbor.talent.managervacations.holiday.service.service.IHolidayService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import java.util.List;

/**
 * @author Jhonatan Soto
 */
@Service
@AllArgsConstructor
public class GetHolidaysByCountryNameUseCase {
    private IHolidayService holidayService;

    private HolidayStructMapper holidayStructMapper;

    private IFeignCountry feignCountry;

    public GetHolidaysResponse execute(String countryName) {
        CountryDto countryDto = feignCountry.getCountryByName(countryName).getBody().getCountry();
        List<Holiday> list = holidayService.findAllByCountryId(countryDto.getId());
        return new GetHolidaysResponse(holidayStructMapper.holidayToHoliday(list));
    }

}

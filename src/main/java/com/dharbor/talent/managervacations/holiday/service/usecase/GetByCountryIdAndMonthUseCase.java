package com.dharbor.talent.managervacations.holiday.service.usecase;

import com.dharbor.talent.managervacations.holiday.service.domain.Holiday;
import com.dharbor.talent.managervacations.holiday.service.dto.response.GetHolidaysResponse;
import com.dharbor.talent.managervacations.holiday.service.mapper.HolidayStructMapper;
import com.dharbor.talent.managervacations.holiday.service.service.IHolidayService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Jhonatan Soto
 */
@Service

@AllArgsConstructor
public class GetByCountryIdAndMonthUseCase {

    private IHolidayService holidayService;

    private HolidayStructMapper holidayStructMapper;

    public GetHolidaysResponse execute(Integer month, Long countryId) {
        List<Holiday> list = holidayService.findByMonthAndCountryId(month, countryId);
        return new GetHolidaysResponse(holidayStructMapper.holidayToHoliday(list));
    }
}

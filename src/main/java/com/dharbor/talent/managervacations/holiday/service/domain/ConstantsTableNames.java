package com.dharbor.talent.managervacations.holiday.service.domain;

/**
 * @author Jhonatan Soto
 */
public final class ConstantsTableNames {
    static class HolidayTable {
        static final String Name = "holiday_table";

        static class Id {
            static final String NAME = "holiday_id";
        }

        static class Reason {
            static final String NAME = "holiday_reason";
            static final int LENGTH = 100;
        }

        static class Date {
            static final String NAME = "holiday_date";
        }

        static class Country {
            static final String NAME = "country_id";
        }
    }

}


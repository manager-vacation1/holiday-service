package com.dharbor.talent.managervacations.holiday.service.dto.request;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

/**
 * @author Henry Villalobos
 */
@Getter
@Setter
public class HolidayRequest {

    private String reason;

    private Date date;
}

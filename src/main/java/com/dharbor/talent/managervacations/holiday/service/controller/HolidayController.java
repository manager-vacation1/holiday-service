package com.dharbor.talent.managervacations.holiday.service.controller;

import com.dharbor.talent.managervacations.holiday.service.dto.request.HolidayRequest;
import com.dharbor.talent.managervacations.holiday.service.dto.response.GetHolidaysResponse;
import com.dharbor.talent.managervacations.holiday.service.dto.response.HolidayExistResponse;
import com.dharbor.talent.managervacations.holiday.service.dto.response.HolidayResponse;
import com.dharbor.talent.managervacations.holiday.service.usecase.*;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

import java.util.Date;

/**
 * @author Jhonatan Soto
 */
@Tag(
        name = "Holiday",
        description = "Operation over holiday"
)
@RestController
@RequestMapping("/holidays")
@AllArgsConstructor
public class HolidayController {
    private CreateHolidayUseCase createHolidayUseCase;

    private GetHolidayByCountryIdUseCase getHolidayByCountryIdUseCase;

    private GetHolidaysByCountryNameUseCase getHolidaysByCountryNameUseCase;

    private GetByCountryIdAndMonthUseCase getByCountryIdAndMonthUseCase;

    private GetHolidayByDateAndCountryId getHolidayByDateAndCountryId;

    @Operation(summary = "Create holiday in a country")
    @PostMapping(value = "/{countryId}")
    public HolidayResponse saveHoliday(@RequestBody HolidayRequest holidayRequest, @PathVariable Long countryId) {
        return createHolidayUseCase.execute(holidayRequest, countryId);
    }

    @Operation(summary = "Get holidays by CountryId")
    @GetMapping("/{countryId}")
    public GetHolidaysResponse getHolidaysByCountryId(@PathVariable Long countryId) {
        return getHolidayByCountryIdUseCase.execute(countryId);
    }

    @Operation(summary = "Get holidays by country name")
    @GetMapping("/countryname/{countryName}")
    public GetHolidaysResponse getHolidaysByCountryName(@PathVariable String countryName) {
        return getHolidaysByCountryNameUseCase.execute(countryName);
    }

    @Operation(summary = "Get holidays by country name")
    @GetMapping("/month/country/")
    public GetHolidaysResponse getHolidaysByMonthAndCountryId(@RequestParam Integer month, Long countryId) {
        return getByCountryIdAndMonthUseCase.execute(month, countryId);
    }

    @GetMapping("/date/country/")
    public HolidayExistResponse getHolidaysByDateAndCountryId(
            @RequestParam("date") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Date date, Long countryId) {
        return getHolidayByDateAndCountryId.execute(date, countryId);
    }


}

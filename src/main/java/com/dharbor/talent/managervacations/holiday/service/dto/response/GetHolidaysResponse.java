package com.dharbor.talent.managervacations.holiday.service.dto.response;

import com.dharbor.talent.managervacations.holiday.service.constant.ResponseConstant;
import com.dharbor.talent.managervacations.holiday.service.domain.CommonResponse;
import com.dharbor.talent.managervacations.holiday.service.dto.HolidayDto;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * @author Henry Villalobos
 */

@Getter
@Setter
public class GetHolidaysResponse extends CommonResponse {

    private List<HolidayDto> holidayList;

    public GetHolidaysResponse(List<HolidayDto> holidayList) {
        super(ResponseConstant.StatusCodeResponse.SUCCESS_CODE, ResponseConstant.StatusCodeResponse.SUCCESS_MSG);
        this.holidayList = holidayList;
    }
}

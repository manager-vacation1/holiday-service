package com.dharbor.talent.managervacations.holiday.service.dto.response;

import com.dharbor.talent.managervacations.holiday.service.constant.ResponseConstant;
import com.dharbor.talent.managervacations.holiday.service.domain.CommonResponse;
import lombok.Getter;
import lombok.Setter;

/**
 * @author Jhonatan Soto
 */
@Getter
@Setter
public class HolidayExistResponse extends CommonResponse {

    private boolean holidayExist;

    public HolidayExistResponse(boolean holidayExist) {
        super(ResponseConstant.StatusCodeResponse.SUCCESS_CODE, ResponseConstant.StatusCodeResponse.SUCCESS_MSG);
        this.holidayExist = holidayExist;
    }
}

package com.dharbor.talent.managervacations.holiday.service.usecase;

import com.dharbor.talent.managervacations.holiday.service.common.Message;
import com.dharbor.talent.managervacations.holiday.service.domain.Holiday;
import com.dharbor.talent.managervacations.holiday.service.dto.CountryDto;
import com.dharbor.talent.managervacations.holiday.service.dto.HolidayDto;
import com.dharbor.talent.managervacations.holiday.service.dto.request.HolidayRequest;
import com.dharbor.talent.managervacations.holiday.service.dto.response.CountryResponse;
import com.dharbor.talent.managervacations.holiday.service.dto.response.HolidayResponse;
import com.dharbor.talent.managervacations.holiday.service.exception.BadRequestException;
import com.dharbor.talent.managervacations.holiday.service.exception.ResourceNotFound;
import com.dharbor.talent.managervacations.holiday.service.mapper.HolidayStructMapper;
import com.dharbor.talent.managervacations.holiday.service.service.IFeignCountry;
import com.dharbor.talent.managervacations.holiday.service.service.IHolidayService;
import com.dharbor.talent.managervacations.holiday.service.utils.Utils;
import feign.FeignException;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class CreateHolidayUseCase {

    private IHolidayService holidayService;

    private IFeignCountry feignCountry;

    private HolidayStructMapper holidayStructMapper;

    private Message message;

    public HolidayResponse execute(HolidayRequest holidayRequest, Long countryId) {
        validateAttributes(holidayRequest);
        validateCountry(countryId);
        CountryDto countryDto = feignCountry.getCountryById(countryId).getBody().getCountry();
        Holiday holidayBuild = buildHoliday(holidayRequest, countryDto);
        Holiday holiday = holidayService.save(holidayBuild);
        return buildHolidayResponse(holiday);

    }

    private Holiday buildHoliday(HolidayRequest holidayRequest, CountryDto country) {
        Holiday holiday = new Holiday();
        holiday.setReason(holidayRequest.getReason());
        holiday.setDate(holidayRequest.getDate());
        holiday.setCountryId(country.getId());
        return holiday;
    }

    private HolidayResponse buildHolidayResponse(Holiday holiday) {
        HolidayDto holidayDto = holidayStructMapper.holidayToHolidayDto(holiday);
        return new HolidayResponse(holidayDto);
    }

    private void validateCountry(Long countryId) {
        try {
            feignCountry.getCountryById(countryId).getBody();
        } catch (FeignException e) {
            throw new ResourceNotFound(message.getMessage("NoExist.Country.code.message"));
        }

    }

    private void validateAttributes(HolidayRequest holidayRequest){
        if (Utils.isNullOrEmpty(holidayRequest.getReason())) {
            throw new BadRequestException(message.getMessage("Reason.Country.Not.Null.Or.Empty"));
        }
        if (Utils.isNullOrEmpty(holidayRequest.getDate())) {
            throw new BadRequestException(message.getMessage("Date.Country.Not.Null.Or.Empty"));
        }
    }
}

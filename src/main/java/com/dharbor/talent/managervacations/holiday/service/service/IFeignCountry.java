package com.dharbor.talent.managervacations.holiday.service.service;

import com.dharbor.talent.managervacations.holiday.service.dto.response.CountryResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * @author Jhonatan Soto
 */
@FeignClient(name = "country-service")
public interface IFeignCountry {
    @GetMapping("/countries/{countryId}")
    ResponseEntity<CountryResponse> getCountryById(@PathVariable Long countryId);

    @GetMapping("/countries/countryname/{countryName}")
    ResponseEntity<CountryResponse> getCountryByName(@PathVariable String countryName);
}

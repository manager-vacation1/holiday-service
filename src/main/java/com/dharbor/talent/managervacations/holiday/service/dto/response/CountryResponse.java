package com.dharbor.talent.managervacations.holiday.service.dto.response;

import com.dharbor.talent.managervacations.holiday.service.dto.CountryDto;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Jhonatan Soto
 */
@Getter
@Setter
@NoArgsConstructor
public class CountryResponse extends CommonResponse {
    private CountryDto country;
}

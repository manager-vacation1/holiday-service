package com.dharbor.talent.managervacations.holiday.service.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

/**
 * @author Jhonatan Soto
 */
@Getter
@Setter
public class HolidayDto {
    private Long id;

    private Date date;

    private String reason;

    private Long countryId;
}

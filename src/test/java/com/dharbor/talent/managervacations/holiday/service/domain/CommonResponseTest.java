package com.dharbor.talent.managervacations.holiday.service.domain;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

class CommonResponseTest {
    @Test
    void testConstructor() {
        CommonResponse actualCommonResponse = new CommonResponse("Status Code", "Not all who wander are lost");
        actualCommonResponse.setMessage("Not all who wander are lost");
        actualCommonResponse.setStatusCode("Status Code");
        assertEquals("Not all who wander are lost", actualCommonResponse.getMessage());
        assertEquals("Status Code", actualCommonResponse.getStatusCode());
    }
}


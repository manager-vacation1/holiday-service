package com.dharbor.talent.managervacations.holiday.service.repository;

import com.dharbor.talent.managervacations.holiday.service.domain.Holiday;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
class HolidayRepositoryTest {

    @Autowired
    private HolidayRepository undertest;

    @Test
    void findAllByCountryId() {
        Holiday holiday = new Holiday();
        holiday.setDate(new Date());
        holiday.setReason("vacation-week");
        holiday.setCountryId(1L);
        undertest.save(holiday);

        List<Holiday> result = undertest.findAllByCountryId(holiday.getId());
        assertTrue(result.contains(holiday));
    }

    @Test
    void findByMonthAndCountryIdNull() {
        Holiday holiday = new Holiday();
        holiday.setDate(new Date());
        holiday.setReason("vacation-week");
        holiday.setCountryId(1L);

        int m = holiday.getDate().getMonth();
        List<Holiday> result = undertest.findByMonthAndCountryId(m, holiday.getId());
        assertFalse(result.contains(holiday));
    }

    @Test
    void existsByDateAndCountryId() {
        Holiday holiday = new Holiday();
        holiday.setDate(new Date());
        holiday.setReason("vacation-week");
        holiday.setCountryId(1L);
        undertest.save(holiday);

        Boolean result = undertest.existsByDateAndCountryId(holiday.getDate(), holiday.getCountryId());
        assertTrue(result);
    }
}
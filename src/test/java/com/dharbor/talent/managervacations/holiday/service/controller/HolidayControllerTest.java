package com.dharbor.talent.managervacations.holiday.service.controller;

import com.dharbor.talent.managervacations.holiday.service.dto.request.HolidayRequest;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;

import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author Jhonatan Soto
 */
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, properties = "spring.cloud.config.enabled=false")
@AutoConfigureTestDatabase
@ActiveProfiles("test")
class HolidayControllerTest {

    @Autowired
    private TestRestTemplate client;

    @Test
    @Order(1)
    void testSaveHoliday() {
        HolidayRequest holidayRequest = new HolidayRequest();
        holidayRequest.setReason("Carnavales");
        holidayRequest.setDate(new Date());

        ResponseEntity<String> response =
                client.postForEntity("/holidays/1", holidayRequest, String.class);
        Assertions.assertEquals(200, response.getStatusCodeValue());
        Assertions.assertTrue(response.getBody().contains("\"statusCode\":\"200\",\"message\":\"Success\",\"holiday\""));
    }

    @Test
    @Order(2)
    void testGetHolidaysByCountryId() {
        ResponseEntity<String> response = client.getForEntity("/holidays/1", String.class);
        Assertions.assertEquals(200, response.getStatusCodeValue());
        Assertions.assertTrue(response.getBody().contains("\"message\":\"Success\",\"holidayList\":"));

    }

    @Test
    @Order(3)
    void getHolidaysByCountryName() {
        ResponseEntity<String> response = client.getForEntity("/holidays/countryname/Bolivia", String.class);
        Assertions.assertEquals(200, response.getStatusCodeValue());
        Assertions.assertTrue(response.getBody().contains("\"message\":\"Success\",\"holidayList\":"));
    }

    @Test
    @Order(4)
    void getHolidaysByMonthAndCountryId() {
        ResponseEntity<String> response = client.getForEntity("/holidays/month/country/?month=3&countryId=1", String.class);
        Assertions.assertEquals(200, response.getStatusCodeValue());
        Assertions.assertTrue(response.getBody().contains("\"message\":\"Success\",\"holidayList\":"));
    }

    @Test
    @Order(5)
    void testCountryNotFound() {
        ResponseEntity<String> response = client.getForEntity("/holidays/10", String.class);
        System.out.println(response);

    }


}

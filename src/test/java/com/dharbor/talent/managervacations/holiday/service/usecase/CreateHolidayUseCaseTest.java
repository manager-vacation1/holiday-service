package com.dharbor.talent.managervacations.holiday.service.usecase;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.dharbor.talent.managervacations.holiday.service.common.Message;
import com.dharbor.talent.managervacations.holiday.service.dto.request.HolidayRequest;
import com.dharbor.talent.managervacations.holiday.service.dto.response.CountryResponse;
import com.dharbor.talent.managervacations.holiday.service.exception.BadRequestException;
import com.dharbor.talent.managervacations.holiday.service.mapper.HolidayStructMapper;
import com.dharbor.talent.managervacations.holiday.service.service.IFeignCountry;
import com.dharbor.talent.managervacations.holiday.service.service.IHolidayService;
import feign.FeignException;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ContextConfiguration(classes = {CreateHolidayUseCase.class})
@ExtendWith(SpringExtension.class)
class CreateHolidayUseCaseTest {
    @Autowired
    private CreateHolidayUseCase createHolidayUseCase;

    @MockBean
    private HolidayStructMapper holidayStructMapper;

    @MockBean
    private IFeignCountry iFeignCountry;

    @MockBean
    private IHolidayService iHolidayService;

    @MockBean
    private Message message;

    @Test
    @Disabled("TODO: This test is incomplete")
    void testExecute() {

        when(this.iFeignCountry.getCountryById((Long) any())).thenReturn(new ResponseEntity<>(HttpStatus.CONTINUE));

        HolidayRequest holidayRequest = new HolidayRequest();
        LocalDateTime atStartOfDayResult = LocalDate.of(1970, 1, 1).atStartOfDay();
        holidayRequest.setDate(Date.from(atStartOfDayResult.atZone(ZoneId.of("UTC")).toInstant()));
        holidayRequest.setReason("Just cause");
        this.createHolidayUseCase.execute(holidayRequest, 3L);
    }

    @Test
    @Disabled("TODO: This test is incomplete")
    void testExecute2() {
        when(this.iFeignCountry.getCountryById((Long) any())).thenReturn(null);

        HolidayRequest holidayRequest = new HolidayRequest();
        LocalDateTime atStartOfDayResult = LocalDate.of(1970, 1, 1).atStartOfDay();
        holidayRequest.setDate(Date.from(atStartOfDayResult.atZone(ZoneId.of("UTC")).toInstant()));
        holidayRequest.setReason("Just cause");
        this.createHolidayUseCase.execute(holidayRequest, 3L);
    }

    @Test
    @Disabled("TODO: This test is incomplete")
    void testExecute3() {
        when(this.iFeignCountry.getCountryById((Long) any()))
                .thenReturn(new ResponseEntity<>(new CountryResponse(), HttpStatus.CONTINUE));

        HolidayRequest holidayRequest = new HolidayRequest();
        LocalDateTime atStartOfDayResult = LocalDate.of(1970, 1, 1).atStartOfDay();
        holidayRequest.setDate(Date.from(atStartOfDayResult.atZone(ZoneId.of("UTC")).toInstant()));
        holidayRequest.setReason("Just cause");
        this.createHolidayUseCase.execute(holidayRequest, 3L);
    }

    @Test
    @Disabled("TODO: This test is incomplete")
    void testExecute4() {
        when(this.message.getMessage((String) any())).thenReturn("Not all who wander are lost");
        when(this.iFeignCountry.getCountryById((Long) any())).thenReturn(new ResponseEntity<>(HttpStatus.CONTINUE));
        HolidayRequest holidayRequest = mock(HolidayRequest.class);
        when(holidayRequest.getDate()).thenThrow(mock(FeignException.class));
        when(holidayRequest.getReason()).thenReturn("Just cause");
        doNothing().when(holidayRequest).setDate((Date) any());
        doNothing().when(holidayRequest).setReason((String) any());
        LocalDateTime atStartOfDayResult = LocalDate.of(1970, 1, 1).atStartOfDay();
        holidayRequest.setDate(Date.from(atStartOfDayResult.atZone(ZoneId.of("UTC")).toInstant()));
        holidayRequest.setReason("Just cause");
        this.createHolidayUseCase.execute(holidayRequest, 3L);
    }

    @Test
    void testExecute5() {
        when(this.message.getMessage((String) any())).thenReturn("Not all who wander are lost");
        when(this.iFeignCountry.getCountryById((Long) any())).thenReturn(new ResponseEntity<>(HttpStatus.CONTINUE));
        HolidayRequest holidayRequest = mock(HolidayRequest.class);
        when(holidayRequest.getDate()).thenReturn(null);
        when(holidayRequest.getReason()).thenReturn("Just cause");
        doNothing().when(holidayRequest).setDate((Date) any());
        doNothing().when(holidayRequest).setReason((String) any());
        LocalDateTime atStartOfDayResult = LocalDate.of(1970, 1, 1).atStartOfDay();
        holidayRequest.setDate(Date.from(atStartOfDayResult.atZone(ZoneId.of("UTC")).toInstant()));
        holidayRequest.setReason("Just cause");
        assertThrows(BadRequestException.class, () -> this.createHolidayUseCase.execute(holidayRequest, 3L));
        verify(this.message).getMessage((String) any());
        verify(holidayRequest).getReason();
        verify(holidayRequest).getDate();
        verify(holidayRequest).setDate((Date) any());
        verify(holidayRequest).setReason((String) any());
    }

    @Test
    void testExecute6() {
        when(this.message.getMessage((String) any())).thenReturn("Not all who wander are lost");
        when(this.iFeignCountry.getCountryById((Long) any())).thenReturn(new ResponseEntity<>(HttpStatus.CONTINUE));
        HolidayRequest holidayRequest = mock(HolidayRequest.class);
        when(holidayRequest.getDate()).thenThrow(mock(FeignException.class));
        when(holidayRequest.getReason()).thenReturn("");
        doNothing().when(holidayRequest).setDate((Date) any());
        doNothing().when(holidayRequest).setReason((String) any());
        LocalDateTime atStartOfDayResult = LocalDate.of(1970, 1, 1).atStartOfDay();
        holidayRequest.setDate(Date.from(atStartOfDayResult.atZone(ZoneId.of("UTC")).toInstant()));
        holidayRequest.setReason("Just cause");
        assertThrows(BadRequestException.class, () -> this.createHolidayUseCase.execute(holidayRequest, 3L));
        verify(this.message).getMessage((String) any());
        verify(holidayRequest).getReason();
        verify(holidayRequest).setDate((Date) any());
        verify(holidayRequest).setReason((String) any());
    }
}


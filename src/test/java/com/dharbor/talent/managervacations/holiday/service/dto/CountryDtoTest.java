package com.dharbor.talent.managervacations.holiday.service.dto;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

class CountryDtoTest {
    @Test
    void testConstructor() {
        CountryDto actualCountryDto = new CountryDto();
        actualCountryDto.setCode("Code");
        actualCountryDto.setId(123L);
        actualCountryDto.setName("Name");
        assertEquals("Code", actualCountryDto.getCode());
        assertEquals(123L, actualCountryDto.getId().longValue());
        assertEquals("Name", actualCountryDto.getName());
    }
}

